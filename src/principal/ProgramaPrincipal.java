package principal;
import java.util.Collections;

import modelo.Banco;
import modelo.Cliente;
import modelo.Conta;
import modelo.ContaCorrente;
import modelo.ContaPoupanca;

public class ProgramaPrincipal {

	
	public static void main(String[] args) throws Exception {
		Conta conta01 = new Conta();
		System.out.println(conta01);
		
		conta01.setAgencia(030202);
		conta01.setLimite(10);
		conta01.setNumero(777722);
		conta01.setSaldo(50);
		System.out.println("-------------------------");
		System.out.println(conta01);
		System.out.println("-------------------------");
		Conta conta02 = new Conta(97777, 1, 120, 10);
		System.out.println(conta02);
		System.out.println("--------------------");
		conta01.depositar(40.0);
		System.out.println(conta01);
		
		//Tratando a exception
		try {
			System.out.println(conta02.sacar(9.99999));
		}catch (Exception excecao){
			System.out.println(excecao.getMessage());
		}finally{
			System.out.println("Opera��o de saque solicitada");
		}
		
		System.out.println("Saldo conta 02: "+ conta02.getSaldo());

		System.out.println(conta01.getCliente());
		
		Cliente novoCliente = new Cliente("Joana", 9393939393L,
										 "Rua da curva", 83988880000L);
		conta01.setCliente(novoCliente);
		System.out.println("Apos o setCliente");
		System.out.println(conta01.getCliente());
		
		Conta conta03 = new Conta(3783, 43, 100.0, 10.5);
		System.out.println(conta03.getCliente());
		
		Cliente novoCliente03 = new Cliente("Jose", 88484848L,
				"Rua maria da silva", 83984740000L);
		conta03.setCliente(novoCliente03);
		System.out.println(conta03.getCliente());
		
		//teste
		
		ContaCorrente contaCorr01 = new ContaCorrente
				(1, 234, 1000.1, 100.0, 0.01);
		System.out.println(contaCorr01.getSaldo());
		
		System.out.println("======================================");
		Banco banco01 = new Banco(0.0);
		banco01.adicionarConta(conta01);
		conta01.setSaldo(0.0);
		banco01.adicionarConta(conta02);
		conta02.setSaldo(0.0);
		banco01.adicionarConta(conta03);
		conta03.setSaldo(0.0);
		//associar banco as contas
		conta01.setBanco(banco01);
		conta02.setBanco(banco01);
		conta03.setBanco(banco01);
		
		
		//Conta contaCorrente = new ContaCorrente(7777, 88888, 0.0, 100.0, 0.05);
		//contaCorrente.setBanco(banco01);
	//	banco01.adicionarConta(contaCorrente);
		
		//conta01.depositar(100.0);
		//conta02.depositar(200.0);
		//contaCorrente.depositar(100.0);
		//contaCorrente.depositar(100.0);
		
		System.out.println("Saldo C01: "+conta01.getSaldo());
		//System.out.println("Saldo CCorr: "+contaCorrente.getSaldo());
		System.out.println("LucroBanco: "+ banco01.getLucro());
		System.out.println("Contas do Banco: "+"\n"+banco01.getContas());
		System.out.println("Contas do Banco: "+"\n"+banco01.getContas());
		Collections.sort(banco01.getContas());
		System.out.println("Contas do Banco: "+"\n"+banco01.getContas());
		
		ContaPoupanca contaP = new ContaPoupanca(332, 5445, 0, 10, 0.01);
		contaP.setBanco(banco01);
		contaP.depositar(1000);
		System.out.println(contaP);
		System.out.println(banco01.getLucro());
		
	}
}
