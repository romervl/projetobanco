package bd;
import java.util.ArrayList;
import java.util.List;

import modelo.Banco;
public class BancoServicoBD {

	private static List<Banco> bDbancos;
	
	public BancoServicoBD() {
		bDbancos = new ArrayList<Banco>();
	}
	
	public static boolean salvar(String nome, double lucro) {
		Banco bancoAtual = new Banco(nome,lucro);
		try {
			bDbancos.add(bancoAtual);
			return true;
		} catch (Exception e) {
			return false;
		}
		//"insert into banco values("+nome+", "+lucro+");";
		
	}

	public static List<Banco> getbDbancos() {
		return bDbancos;
	}
	
	

}
