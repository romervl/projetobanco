package modelo;

public class ContaCorrente extends Conta{
	
	private double taxa;
	
	public ContaCorrente(int numero, int agencia,
			double saldo, double limite, double taxa) {
		super(numero,agencia,saldo,limite);
		this.taxa = taxa;		
	}

	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	public boolean depositar(double valor) throws Exception {
		double lucroDoBanco = valor*this.taxa;//5
						// 95 <= 100 - 5
		double valorParaDeposito =valor -lucroDoBanco;
		Banco bancoAtual = this.getBanco();
		//bancoAtual.setLucro(bancoAtual.getLucro()+lucroDoBanco);
		bancoAtual.adicionarLucro(lucroDoBanco);
		return super.depositar(valorParaDeposito);
	}
	
	
	@Override
	public boolean sacar(double valor) throws Exception{
		double taxaAtual = valor*0.01;
		valor = valor - taxa;
		this.getBanco().adicionarLucro(taxaAtual);
		super.sacar(valor);	
		return true;
	}

	@Override
	public String toString() {
		return "ContaCorrente [taxa=" + taxa + "]";
	}
}
