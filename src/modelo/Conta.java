package modelo;

public class Conta  implements Comparable  {

	private int numero;
	private int agencia;
	private double saldo;
	private double limite;
	private Cliente cliente;
	private Banco banco;

	
	// Construtor vazio
	public Conta() {}
	
	// Construtor com atributos
	public Conta(int numero, int agencia, double saldo, double limite) {
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
		this.limite = limite;
	}

	public Conta(int numero, int agencia) {
		this.numero = numero;
		this.agencia = agencia;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public boolean depositar(double valor) throws Exception {
		if (valor > 0.0) {
			this.saldo += valor;
			return true;
		}
		return false;

	}

	//Lancando a exception
	public boolean sacar(double valor) throws Exception {
		if (valor <= this.limite && valor <= saldo) {
			if(valor<10) {
				Exception exc = new Exception("Nao pode sacar valor menor que 10 reais");
				throw exc;
			}
			this.saldo -= valor;
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Conta [numero=" + numero + ", agencia=" + agencia + ", saldo=" + saldo + ", limite=" + limite + "]";
	}

	@Override
	public int compareTo(Object o) {
		Conta contaAtual = (Conta) o;
		if(this.getSaldo()>contaAtual.getSaldo()) return 1;
		else if(this.getSaldo()<contaAtual.getSaldo()) return -1;
		return 0;
	}

}
