package modelo;

import java.util.ArrayList;
import java.util.List;

public class Banco {

	private static final String MENSAGEM_ERRO_LUCRO_NEGATIVO="Nao pode lucro zero ou negativo";
	private long id;
	private String nome;
	private double lucro;
	private List<Conta> contas;
	

	public Banco(long id, String nome, double lucro) {
		this.id = id;
		this.nome = nome;
		this.lucro = lucro;
		this.contas = new ArrayList<Conta>();
	}
	
	public Banco(String nome, double lucro) {
		this.nome = nome;
		this.lucro = lucro;
		this.contas = new ArrayList<Conta>();
	}
	
	public Banco(double lucro) {
		this.lucro = lucro;
		this.contas = new ArrayList<Conta>();
	}

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public double getLucro() {
		return this.lucro;
	}

	public void setLucro(double lucro) {
		this.lucro = lucro;
	}
	
	public void adicionarLucro(double novoLucro) throws Exception {
		if(novoLucro<=0) {
			Exception exc = new Exception(MENSAGEM_ERRO_LUCRO_NEGATIVO);
			throw exc;
		}
		this.lucro+=novoLucro;
	}
	
	public void diminuirLucro(double novoLucro){
		this.lucro-=novoLucro;
	}

	@Override
	public String toString() {
		return "Banco [lucro=" + lucro + "]";
	}
	
	public boolean adicionarConta(Conta novaConta) {
		return this.contas.add(novaConta);
	}
	
	public boolean removerConta(Conta umaConta) {
		return this.contas.remove(umaConta);
	}
	
	public List<Conta> getContas() {
		return this.contas;
	}
	
}
