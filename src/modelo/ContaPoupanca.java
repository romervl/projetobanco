package modelo;

public class ContaPoupanca extends Conta {
	
	private double rendimento;


	public ContaPoupanca(int numero, int agencia,
			double saldo, double limite, double rendimento) {
		super(numero, agencia, saldo, limite);
		this.rendimento = rendimento;
	}
	
	public boolean depositar(double valor) throws Exception {
		Banco bancoAtual = this.getBanco();
		double valorRendimento = valor*this.rendimento;
		bancoAtual.diminuirLucro(valorRendimento);
		return super.depositar(valor+valorRendimento);
		
	}

	public double getRendimento() {
		return rendimento;
	}

	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}

}
